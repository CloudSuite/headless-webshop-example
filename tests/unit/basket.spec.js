import {shallowMount, createLocalVue} from '@vue/test-utils'
import Vuex from 'vuex'
import Basket from '../../src/views/basket'
import BootstrapVue from 'bootstrap-vue'


const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(BootstrapVue);
localVue.filter('currency', (value) => {
  if (!value) return '';
  value = value.toString();
  return "€" + value + ".0";
});
describe('basket', () => {
  let getters;
  let store;

  beforeEach(() => {
    getters = {
      getBasketLines: () => [
        {
          product: {
            id: 1,
            name: 'object1',
            sale_price: 200,
            currency_symbol: '€',
            picture: {
              normal: 'normal.jpg'
            }
          },
          quantity: 2
        }, {
          product: {
            id: 2,
            name: 'object2',
            sale_price: 300,
            currency_symbol: '€',
            picture: {
              normal: 'normal.jpg'
            }
          },
          quantity: 1
        }],
      getBasket: () => {
        return {total_price: 700, total_vat: 2100}
      }
    };
    store = new Vuex.Store({
      getters
    })
  });

  it('getTotalAmountItems should return 7.0', () => {
    const wrapper = shallowMount(Basket, {store, localVue});
    const totals = wrapper.find('.checkout-page__item__total').text().split('\n')
    const totalPrice = totals[0].trim();
    const totalVAT = totals[1].trim();
    expect(totalPrice).toBe('Total: €7.0');
    expect(totalVAT).toBe('VAT: €21.0');
  });
});
