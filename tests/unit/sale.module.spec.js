import moxios from "moxios";
import saleModule from '../../src/store/modules/sale/sale.module'
import store from '../../src/store/store'


describe('sale.module', () => {
  let state;
  let emptyState;
  beforeEach(() => {
    moxios.install();
    state = {
      sale: {
        total_vat: 0,
        total_amount: 10000,
        payment_method: null,
        payment_status: "account",
        sale_lines: [],
        state: "confirmed",
        invoice_address: {},
        sale_reference: "1234567890",
        currency_symbol: "$",
        service_lines: [],
        shipment_address: {},
        user: {
          first_name: "John",
          last_name: 'Doe',
          telephone_number: "60606060606060",
          email: "john.doe@cloudsuite.com",
          mobile_number: null
        }
      }
    };
    emptyState = {
      sale: null
    }
  });
  afterEach(() => {
    moxios.uninstall();
  });
  describe('Getters', () => {
    it('getSale should get the latest sale', () => {
      const result = saleModule.getters.getSale(state);
      expect(result).toEqual(state.sale)
    })
  });
  describe('Actions', () => {
    it('fetchSale should retrieve the latest sale', (done) => {
      moxios.stubRequest('/api/v1/sale/123/', {response: state.sale});
      moxios.wait(async () => {
        await store.dispatch('fetchSale', '123');
        expect(store.state.saleModule.sale).toEqual(state.sale);
        done();
      })
    });
  });
  describe('Mutations', () => {
    it('setSale should set the sale state', () => {
      saleModule.mutations.setSale(emptyState, state.sale);
      expect(emptyState.sale).toEqual(state.sale)
    })
  })
});
