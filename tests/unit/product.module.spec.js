import productsModule from '../../src/store/modules/products/products.module'
import store from '../../src/store/store'
import moxios from 'moxios'

describe('products.module', () => {
  let state;
  let emptyState;
  let productResponse;
  let productAttributes;
  let pictures;
  beforeEach(() => {
    moxios.install();
    pictures = [
      {
        identifier: 'picture-1',
        type: 'product_image',
        name: 'picture-1',
        urls: {
          "small": "https://localhost:5001/static/uploads/pictures/small/picture.jpg",
          "mini": "https://localhost:5001/static/uploads/pictures/mini/picture.jpg",
          "large": "https://localhost:5001/static/uploads/pictures/large/picture.jpg",
          "normal": "https://localhost:5001/static/uploads/pictures/normal/picture.jpg"
        },
        sequence: "1"
      }
    ];
    productAttributes = [{
      picture: null,
      description: null,
      value: null,
      id: 1,
      uom: null,
      name: 'product-attribute1'
    }];
    productResponse = {
      count: 0,
      next_url: null,
      previous_url: null,
      results: [
        {
          id: 1,
          code: 'product-1',
          description: 'some description',
          name: 'product-name1',
          slug: 'slug-product1',
          tags: [],
          pictures: pictures,
          product_attributes: productAttributes,
          last_modified: null,
          currency_symbol: "$",
          sale_price: 1000,
          out_of_stock: false
        },
        {
          id: 2,
          code: 'product-2',
          description: 'some description',
          name: 'product-name2',
          slug: 'slug-product2',
          tags: [],
          pictures: pictures,
          product_attributes: productAttributes,
          last_modified: null,
          currency_symbol: "$",
          sale_price: 1000,
          out_of_stock: false
        }
      ]
    };
    state = {
      productData: productResponse.results,
      product: productResponse.results[0]
    };

    emptyState = {
      productData: [],
      product: null
    };
  });
  afterEach(() => {
    moxios.uninstall()
  });
  describe('Getters', () => {
    it('getProductById should return correct product', () => {
      const result = productsModule.getters.getProductById(state)(1);
      expect(result).toEqual({
        id: 1,
        code: 'product-1',
        description: 'some description',
        name: 'product-name1',
        slug: 'slug-product1',
        tags: [],
        pictures: pictures,
        product_attributes: productAttributes,
        last_modified: null,
        currency_symbol: "$",
        sale_price: 1000,
        out_of_stock: false
      })
    });
    it('getAllProducts should return all products', () => {
      const result = productsModule.getters.getAllProducts(state);
      expect(result).toEqual(state.productData)
    });
    it('getRangeOfProducts should return 2 products', () => {
      const result = productsModule.getters.getRangeOfProducts(state)(2);
      expect(result).toHaveLength(2);
    })
    it('getRangeOfProducts should return empty array', () => {
      const result = productsModule.getters.getRangeOfProducts(emptyState)(2);
      expect(result).toEqual([])
    });
  });
  describe('Actions', () => {

    it('fetchProducts retrieves products', done => {
      moxios.stubRequest('/api/v1/product/', {response: productResponse});
      moxios.wait(async () => {
        await store.dispatch('fetchProducts');
        expect(store.state.productModule.productData).toEqual(state.productData);
        done()
      })
    });
    it('fetchProducts should retrieve products per tree', (done) => {
      moxios.stubRequest('/api/v1/product/?tree_id=1', {response: productResponse});
      moxios.wait(async () => {
        await store.dispatch('fetchProducts', {tree_id: 1});
        expect(store.state.productModule.productData).toEqual(state.productData);
        done()
      })
    });
    it('fetchProducts should retrieve products per page and limit', (done) => {
      productResponse.results = productResponse.results.slice(0,1);
      moxios.stubRequest('/api/v1/product/?page=1&limit=1', {response: productResponse});
      moxios.wait(async () => {
        await store.dispatch('fetchProducts', {page: 1, limit: 1});
        expect(store.state.productModule.productData[0]).toEqual({
          id: 1,
          code: 'product-1',
          description: 'some description',
          name: 'product-name1',
          slug: 'slug-product1',
          tags: [],
          pictures: pictures,
          product_attributes: productAttributes,
          last_modified: null,
          currency_symbol: "$",
          sale_price: 1000,
          out_of_stock: false
        });
        done()
      })
    });
    it('fetchProductById can be called', done => {
      moxios.stubRequest('/api/v1/product/1/', {response: productResponse.results[0]});
      moxios.wait(async () => {
        await store.dispatch('fetchProductById', 1);
        expect(store.state.productModule.product).toEqual(productResponse.results[0]);
        done()
      })
    });
  });
  describe('Mutations', () => {
    it('setAllProducts should add all products to productList', () => {
      const products = productResponse.results;
      productsModule.mutations.setAllProducts(emptyState, products);
      const expectedState = {
        productData: products,
        product: null,
      };
      expect(expectedState).toEqual(emptyState)
    });
    it('setProduct should add a new product', () => {
     const product = productResponse.results[0];
      productsModule.mutations.setProduct(emptyState, product);
      const expectedState = {
        productData: [],
        product: product
      };
      expect(expectedState).toEqual(emptyState)
    })
  })
});
