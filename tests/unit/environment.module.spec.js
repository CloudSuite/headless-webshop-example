import environmentModule from '../../src/store/modules/environment/environment.module'
import store from '../../src/store/store'
import moxios from 'moxios'

describe('environment.module', () => {
  let state;
  let emptyState;
  beforeEach(() => {
    moxios.install();
    state = {
      enterpriseDomain: 'nl-nl',
      availableLanguages: [{code: 'nl_NL', id: 1, code_short: 'nl'}, {code: 'de_DE', id: 2, code_short: 'de'}],
      shopLanguageCode: 'nl',
      countries: [{url: 'helloworld/nl-nl/', country: {code: 'NL', name: 'Nederland'}}]
    };
    emptyState = {
      enterpriseDomain: 'nl-nl',
      availableLanguages: [],
      shopLanguageCode: null,
      countries: [],

    };
  });
  afterEach(() => {
    moxios.uninstall()
  });
  describe('Getters', () => {
    it('getEnterpriseDomain should return enterprise domain', () => {
      const result = environmentModule.getters.getEnterpriseDomain(state);
      expect(result).toEqual('nl-nl')
    });
    it('getAvailableLanguages should return available languages', () => {
      const result = environmentModule.getters.getAvailableLanguages(state);
      expect(result).toEqual(state.availableLanguages)
    });
    it('getShopLanguageCode should return shop language code', () => {
      const result = environmentModule.getters.getShopLanguageCode(state);
      expect(result).toEqual(state.shopLanguageCode)
    });
     it('getCountries should return the countries', () => {
      const result = environmentModule.getters.getCountries(state);
      expect(result).toEqual(state.countries)
    });
  });
  describe('Actions', () => {
    it('updateShopLanguageCode should update the shop language code', async () => {
      await store.dispatch('updateShopLanguageCode', state.availableLanguages[0]);
      expect(store.state.environmentModule.shopLanguageCode).toEqual('nl_NL');
    });
    it('fetchAvailableLanguagesOfShop should update the available languages', (done) => {
      moxios.stubRequest('/api/v1/domain/available-languages/', {response: state.availableLanguages});
      moxios.wait(async () => {
        await store.dispatch('fetchAvailableLanguagesOfShop');
        expect(store.state.environmentModule.availableLanguages).toEqual(state.availableLanguages);
        done()
      })
    });
    it('fetchCountries should fetch the countries of the enterprise domain', (done) => {
      moxios.stubRequest('/api/v1/domain/countries/', {response: state.countries});
      moxios.wait(async () => {
        await store.dispatch('fetchCountries');
        expect(store.state.environmentModule.countries).toEqual(state.countries);
        done()
      })
    })
  });
  describe('Mutations', () => {
    it('setEnterpriseDomain', () => {
      environmentModule.mutations.setEnterpriseDomain(emptyState, 'de-de');
      expect(emptyState.enterpriseDomain).toEqual('de-de')
    });
    it('setAvailableLanguages', () => {
      environmentModule.mutations.setAvailableLanguages(emptyState, state.availableLanguages);
      expect(emptyState.availableLanguages).toEqual(state.availableLanguages)
    });
    it('setShopLanguageCode', () => {
      environmentModule.mutations.setShopLanguageCode(emptyState, 'nl_NL');
      expect(emptyState.shopLanguageCode).toEqual('nl_NL')
    });
     it('setAvailableCountries', () => {
      environmentModule.mutations.setAvailableCountries(emptyState, 'Bolivia');
      expect(emptyState.countries).toEqual('Bolivia')
    });
  })
});