import basketModule from '../../src/store/modules/basket/basket.module'
import store from '../../src/store/store'
import moxios from 'moxios'
import VueCookies from 'vue-cookies'


describe('basket.module', () => {
  let state;
  let emptyState;
  let product;
  let basketLine;
  beforeEach(() => {
    moxios.install();
    state = {
      basket: {
        basket_UUID: 'test1',
        lines: [
          {
            id: 1,
            product: {
              id: 1,
              name: 'object1',
              sale_price_no_format: 2
            },
            quantity: 1
          }, {
            id: 2,
            product: {
              id: 2,
              name: 'object2',
              sale_price_no_format: 3
            },
            quantity: 1
          },
          {
            id: 3,
            product: {
              id: 3,
              name: 'object3',
              sale_price_no_format: 3
            },
            quantity: 1
          },
          {
            id: 4,
            product: {
              id: 4,
              name: 'object4',
              sale_price_no_format: 3
            },
            quantity: 1
          },
          {
            id: 5,
            product: {
              id: 5,
              name: 'object5',
              sale_price_no_format: 3
            },
            quantity: 1
          }
        ]
      }
    };
    emptyState = {
      basket: null
    };
    product = {
      id: 1,
      name: 'object1',
      sale_price_no_format: 2
    };
    basketLine = {
      product,
      id: 1,
      quantity: 2
    }


  });

  afterEach(() => {
    moxios.uninstall();
    VueCookies.set('basket', undefined)
  });

  describe('Getters', () => {
    it('getTotalOfProducts should be 5', () => {
      const result = basketModule.getters.getTotalOfProductsBasket(state);
      expect(result).toEqual(5)
    });
    it('getBasketLines should be the same as the state', () => {
      const result = basketModule.getters.getBasketLines(state);
      expect(result).toEqual(state.basket.lines)
    });
  });
  describe('Actions', () => {
    it('createBasket will create a new basket', (done) => {
      moxios.stubRequest('/api/v1/basket/', {response: {basket_UUID: 'test1', lines: []}});
      moxios.wait(async () => {
        await store.dispatch('createBasket');
        expect(store.state.basketModule.basket).toEqual({basket_UUID: 'test1', lines: []});
        expect(VueCookies.get('basket')).toEqual('test1');
        done()
      })

    });
    it('initiateBasket can be called and fetches already set basket', (done) => {
      moxios.stubRequest('/api/v1/basket/test1/', {response: {basket_UUID: 'test1', lines: []}});
      moxios.wait(async () => {
        VueCookies.set('basket', 'test1');
        await store.dispatch('initiateBasket');
        expect(store.state.basketModule.basket).toEqual({basket_UUID: 'test1', lines: []});
        done()
      })

    });
    it('initiateBasket can be called and creates new basket', (done) => {
      moxios.stubRequest('/api/v1/basket/undefined/', {status: 400, response: 'No basket found'});
      moxios.stubRequest('/api/v1/basket/', {response: {basket_UUID: 'test1', lines: []}});
      moxios.wait(async () => {
        await store.dispatch('initiateBasket');
        expect(store.state.basketModule.basket).toEqual({basket_UUID: 'test1', lines: []});
        done()
      })

    });
    it('addProductToBasket can add a new product', (done) => {
      moxios.stubRequest('/api/v1/basket/test1/line/', {response: {basket_UUID: 'test1', lines: [basketLine]}});
      moxios.wait(async () => {
        store.commit('updateBasket', {basket_UUID: 'test1', lines: []});
        await store.dispatch('addProductToBasket', {product: product});
        expect(store.state.basketModule.basket).toEqual({basket_UUID: 'test1', lines: [basketLine]});
        done();
      })
    });
    it('addProductToBasket can update a product', (done) => {
      basketLine = {
        id: 5,
        product: product,
        quantity: 6
      };
      const newState = {basket_UUID: 'test1', lines: [basketLine]};
      moxios.stubRequest('/api/v1/basket/test1/line/5/', {response: newState});
      moxios.wait(async () => {
        store.commit('updateBasket', newState)
        await store.dispatch('addProductToBasket', {product: product});
        expect(store.state.basketModule.basket.lines[0].quantity).toEqual(newState.lines[0].quantity);
        done()
      })
    });
    it('addNewProductToBasket can be called', (done) => {
      moxios.stubRequest('/api/v1/basket/test1/line/', {response: {basket_UUID: 'test1', lines: [basketLine]}});
      moxios.wait(async () => {
        store.commit('updateBasket', {basket_UUID: 'test1', lines: []});
        await store.dispatch('addNewProductToBasket', product);
        expect(store.state.basketModule.basket).toEqual({basket_UUID: 'test1', lines: [basketLine]});
        done()
      });
    });
    it('increaseProductQuantityInBasket can be called', (done) => {
      moxios.stubRequest('/api/v1/basket/test1/line/1/', {response: {basket_UUID: 'test1', lines: [basketLine]}});
      moxios.wait(async () => {
        store.commit('updateBasket', {basket_UUID: 'test1', lines: [basketLine]});
        await store.dispatch('increaseProductQuantityInBasket', basketLine)
        expect(store.state.basketModule.basket.lines[0].quantity).toEqual(2);
        done()
      });
    });
    it('removeBasketLineFromBasket removes a basket line', (done) => {
      moxios.stubRequest('/api/v1/basket/test1/line/1/', {response: {basket_UUID: 'test1', lines: []}});
      moxios.wait(async () => {
        store.commit('updateBasket', {basket_UUID: 'test1', lines: [basketLine]});
        await store.dispatch('increaseProductQuantityInBasket', basketLine);
        expect(store.state.basketModule.basket.lines.length).toEqual(0);
        done()
      })
    });
    it('fetchBasket can be called', (done) => {
      moxios.stubRequest('/api/v1/basket/test1/', {response: {basket_UUID: 'test1', lines: []}});
      moxios.wait(async () => {
        await store.dispatch('fetchBasket', 'test1');
        expect(store.state.basketModule.basket).toEqual({basket_UUID: 'test1', lines: []});
        done()
      })
    });
    it('fetchBasket creates a new basket when basket UUID is not valid', (done) => {
      moxios.stubRequest('/api/v1/basket/invalid/', {status: 404, response: 'error'});
      moxios.stubRequest('/api/v1/basket/', {response: {basket_UUID: 'test2', lines: []}});
      moxios.wait(async () => {
        await store.dispatch('fetchBasket', 'invalid');
        expect(store.state.basketModule.basket).toEqual({basket_UUID: 'test2', lines: []});
        done()
      })
    });
    it('deleteBasket can be called', (done) => {
      moxios.stubRequest('/api/v1/basket/test1/', {response: {}});
      moxios.wait(async () => {
        store.commit('updateBasket', {basket_UUID: 'test1', lines: [basketLine]});
        await store.dispatch('deleteBasket', 'test1');
        expect(store.state.basketModule.basket).toEqual(null);
        done()
      })
    })
    it('goToCheckout can be called', (done) => {
      moxios.stubRequest('/api/v1/basket/test1/', {
        response: {
          basket_UUID: 'test1',
          lines: [basketLine],
          checkout: 'checkout'
        }
      });
      moxios.wait(async () => {
        store.commit('updateBasket', {basket_UUID: 'test1'});
        const result = await store.dispatch('goToCheckout', 'test1');
        expect(result).toEqual('checkout');
        done()
      })
    });
  });
  describe('Mutations', () => {
    it('deleteBasketCookie should delete the basket cookie', () => {
      VueCookies.set('basket', 'test');
      basketModule.mutations.deleteBasketCookie();
      let result = VueCookies.get('basket');
      expect(result).toEqual(null)
    });
    it('updateBasket mutation should add a product to the productList', () => {

      basketModule.mutations.updateBasket(emptyState, {
        basket: {
          basket_UUID: 'test',
          lines: []
        }
      });
      expect(emptyState.basket).toEqual({
        basket: {
          basket_UUID: 'test',
          lines: []
        }
      })
    });
    it('deleteBasket should set basket to an empty object', () => {
      const deleteState = {
        basket: {
          basket_UUID: 'test1',
          lines: [
            {
              product: {
                id: 1,
                name: 'object1',
                sale_price_no_format: 2
              },
              quantity: 1
            }]
        }
      };
      basketModule.mutations.deleteBasket(deleteState);
      expect(deleteState.basket).toEqual(null);
    });
  });
  describe('Helpers', () => {
    it('checkBasketUUIDIsNullOrUndefined is True when basket is null', () => {
      const basket = null;
      const result = basketModule.helpers.checkBasketUUIDIsNullOrUndefined(basket);
      expect(result).toBe(true);
    });
    it('checkBasketUUIDIsNullOrUndefined is True when basket_UUID is null', () => {
      const basket = {basket_UUID: null};
      const result = basketModule.helpers.checkBasketUUIDIsNullOrUndefined(basket);
      expect(result).toBe(true);
    });
    it('checkBasketUUIDIsNullOrUndefined is True when basket_UUID is undefined', () => {
      const basket = {basket_UUID: undefined};
      const result = basketModule.helpers.checkBasketUUIDIsNullOrUndefined(basket);
      expect(result).toBe(true);
    });
    it('checkBasketUUIDIsNullOrUndefined is False when basket_UUID is defined', () => {
      const basket = {basket_UUID: '123'};
      const result = basketModule.helpers.checkBasketUUIDIsNullOrUndefined(basket);
      expect(result).toBe(false);
    });
  });
  it('getProductIdsInBasket should return product ids', () => {
    store.commit('updateBasket', state.basket);
    const result = basketModule.helpers.getProductIdsInBasket();
    expect(result).toEqual([1, 2, 3, 4, 5])
  });
  it('updateProductQuantityPayload should update the quantity of a product with 1', () => {
    store.commit('updateBasket', {basket_UUID: 'test1', lines: [basketLine]});
    const result = basketModule.helpers.updateProductQuantityPayload(product)
    expect(result).toEqual({product, 'quantity': 3})
  });
  it('getBasketLine find basketline with product id', () => {
    store.commit('updateBasket', {basket_UUID: 'test1', lines: [basketLine]});
    const result = basketModule.helpers.getBasketLine(1);
    expect(result).toEqual(basketLine)

  })

});

