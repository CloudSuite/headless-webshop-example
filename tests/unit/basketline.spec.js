import {shallowMount, createLocalVue} from '@vue/test-utils'
import Vuex from 'vuex'
import BasketLine from '../../src/components/basketline.vue'
import BootstrapVue from 'bootstrap-vue'


const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(BootstrapVue);
localVue.filter('currency', (value) => {
  if (!value) return '';
  value = value.toString();
  return "€" + value + ".0";
});
describe('basketline', () => {
  let store;
  let actions;
  let basketline;

  beforeEach(() => {

    actions = {
      removeBasketLineFromBasket: jest.fn()
    };

    basketline = {
      product: {
        id: 1,
        name: 'object1',
        sale_price: 200,
        currency_symbol: "€",
        pictures: [
          {
            urls: {
              normal: 'normal.jpg',
              mini: 'mini.jpg'
            },
            type: 'product_image'
          }
        ]
      },
      quantity: 2
    };

    store = new Vuex.Store({
      actions
    });
  });

  it('getTotalAmountItem should return 4.0', () => {
    const wrapper = shallowMount(BasketLine, {store, localVue, propsData: {basketline: basketline}});
    expect(wrapper.find('.checkout-page__total-amount-product').text()).toBe('€4.0')
  });
});
