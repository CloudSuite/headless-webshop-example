import treesModule from '../../src/store/modules/trees/trees.module'
import store from '../../src/store/store'
import moxios from 'moxios'


describe('tree.module', () => {
  let TreeListResponse;
  let TreeDetailResponse;
  let emptyState;
  let state;
  beforeEach(() => {
    moxios.install();
    TreeListResponse = {
      count: 0,
      next_url: null,
      previous_url: null,
      results: [
        {
          id: 1,
          name: 'tree1',
          slug: 'slug-tree1',
          parent_id: null,
          children: [
            {
              id: 2,
              name: 'tree2',
              slug: 'slug-tree2'
            }
          ],
          assets: []
        },
        {
          id: 2,
          name: 'tree2',
          slug: 'slug-tree2',
          parent_id: 1,
          children: [{
            id: 3,
            name: 'tree3',
            slug: 'slug-tree3'
          }],
          assets: []
        },
        {
          id: 3,
          name: 'tree3',
          slug: 'slug-tree3',
          parent_id: 2,
          children: [],
          assets: []
        }
      ]
    };
    TreeDetailResponse = {
      id: 1,
      name: 'tree1',
      slug: 'slug-tree1',
      parent_id: null,
      children: [
        {
          name: 'tree2',
          slug: 'slug-tree2',
          id: 2
        }
      ],
      assets: []
    };
    state = {
      trees: TreeListResponse.results,
      tree: TreeDetailResponse,
    };

    emptyState = {
      trees: [],
      tree: null,
    };
  });
  afterEach(() => {
    moxios.uninstall()
  });

  describe('Getters', () => {
    it('getTrees', () => {
      const result = treesModule.getters.getTrees(state);
      expect(result).toEqual(
        [{
          id: 1,
          name: 'tree1',
          slug: 'slug-tree1',
          parent_id: null,
          children: [
            {
              id: 2,
              name: 'tree2',
              slug: 'slug-tree2'
            }
          ],
          assets: []
        }, {
          id: 2,
          name: 'tree2',
          slug: 'slug-tree2',
          parent_id: 1,
          children: [{
            id: 3,
            name: 'tree3',
            slug: 'slug-tree3'
          }],
          assets: []
        }, {
          id: 3,
          name: 'tree3',
          slug: 'slug-tree3',
          parent_id: 2,
          children: [],
          assets: []
        }])
    })
  });
  it('getHighestTrees', () => {
    const result = treesModule.getters.getHighestTrees(state);
    expect(result).toEqual([
      {
        id: 2,
        name: "tree2",
        slug: "slug-tree2",
      },
    ])
  });
  describe('Actions', () => {
    it('fetchTrees fetches the trees', done => {
      moxios.stubRequest('/api/v1/tree/?limit=1000', {response: TreeListResponse});
      moxios.wait(async () => {
        await store.dispatch('fetchTrees');
        expect(store.state.treesModule.trees).toEqual(state.trees);
        done()
      })
    });
    it('fetchTreeByID can be called', done => {
      moxios.stubRequest('/api/v1/tree/1/', {response: TreeDetailResponse});
      moxios.wait(async () => {
        await store.dispatch('fetchTreeById', 1);
        expect(store.state.treesModule.tree).toEqual(state.tree);
        done()
      })
    });
    it('fetchRangeOfTrees should return childtrees', async () => {
      store.commit('setTrees', state.trees);
      const result = await store.dispatch('fetchRangeOfTrees', [1, 2]);
      expect(result).toEqual([{
        assets: [],
        children: [{id: 2, name: "tree2", slug: "slug-tree2"}],
        id: 1,
        name: "tree1",
        parent_id: null,
        slug: "slug-tree1"
      }, {
        assets: [],
        children: [{id: 3, name: "tree3", slug: "slug-tree3"}],
        id: 2,
        name: "tree2",
        parent_id: 1,
        slug: "slug-tree2"
      }])
    })
  });
  describe('Mutations', () => {
    it('setTrees', () => {
      const trees = state.trees;
      treesModule.mutations.setTrees(emptyState, trees);
      expect(emptyState).toEqual({trees: trees, tree: null})
    });
    it('setTree', () => {
      const tree = state.tree;
      treesModule.mutations.setTree(emptyState, tree);
      expect(emptyState).toEqual({trees: [], tree: tree})
    })

  });
});

