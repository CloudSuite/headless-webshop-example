module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
    ? '/headless/'
    : '/',
    devServer: {
        proxy: {
            '/api/v1/': {
                target: process.env.DEVSERVER_PROXY_TARGET,
                changeOrigin: true,
                xfwd: false
            }
        }
    },
    css: {
    loaderOptions: {
      sass: {
        data: `@import "@/styles/global.scss";`
      }
    }
  }
};
