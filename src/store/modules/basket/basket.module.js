import axios from 'axios'

const state = {
  basket: null
};

const getters = {
  getBasket: state => {
    return state.basket;
  },
  getBasketLines: state => {
    return state.basket && state.basket.lines ? state.basket.lines : [];
  },
  getTotalOfProductsBasket: state => {
    if (!state.basket || !state.basket.lines) {
      return 0;
    }
    return state.basket.lines
      .map(basketline => basketline.quantity)
      .filter(quantity => quantity > 0)
      .reduce((prevQuantity, currQuantity) => prevQuantity + currQuantity, 0);
  }
};

const actions = {
    createBasket: ({commit}) => {
      return axios.post('/api/v1/basket/')
        .then(response => {
          commit('updateBasket', response.data);
          window.$cookies.set('basket', response.data.basket_UUID);
        })
        .catch(error => console.log(error))
    },
    initiateBasket: ({dispatch}) => {
      /** Gets current or creates a basket with the Headless API **/
      const basketCookieKey = 'basket';
      let basketUUID = window.$cookies.get(basketCookieKey);
      if (basketUUID) {
        return dispatch('fetchBasket', basketUUID)
      } else {
        return dispatch('createBasket')
      }
    },
    addProductToBasket: ({dispatch}, payload) => {
      /** If the basket UUID is set then it will check if the product exists in the basket or not.
       *  If the product already exists it will increase the products quantity.
       * If the product doesn't exist it will add the product to the basket.
       *
       * If the basket UUID is not set it will initiate a new basket and
       * then will perform the actions as described in the previous
       * **/
      const product = payload.product;
      const productIds = helpers.getProductIdsInBasket();

      if (helpers.checkBasketUUIDIsNullOrUndefined(state.basket)) {
        helpers.initiateBasketAndAddProduct(dispatch, payload);
      } else if (productIds.includes(product.id)) {
        const newPayload = helpers.updateProductQuantityPayload(product);
        return dispatch('increaseProductQuantityInBasket', newPayload);
      } else if (!productIds.includes(product.id)) {
        return dispatch('addNewProductToBasket', product);
      }
    },
    addNewProductToBasket: ({commit}, payload) => {
      const basketLine = {product: payload.id, quantity: 1};
      const addNewProductToBasketRequest = `/api/v1/basket/${state.basket.basket_UUID}/line/`;
      return axios.post(addNewProductToBasketRequest, basketLine)
        .then((response) => {
          commit('updateBasket', response.data);
        })
        .catch(error => console.log(error))
    },
    increaseProductQuantityInBasket: ({commit}, payload) => {
      const productID = payload.product.id;
      const quantity = payload.quantity;
      const basketLine = helpers.getBasketLine(productID);
      const increaseProductQuantityInBasketRequest = `/api/v1/basket/${state.basket.basket_UUID}/line/${basketLine.id}/`;
      const newQuantity = {'quantity': quantity};
      return axios.patch(increaseProductQuantityInBasketRequest, newQuantity)
        .then((response) => {
          commit('updateBasket', response.data);
        })
        .catch(error => console.log(error))
    },
    removeBasketLineFromBasket: ({commit}, basketLineID) => {
      const deleteBasketLineRequest = `/api/v1/basket/${state.basket.basket_UUID}/line/${basketLineID}/`;
      return axios.delete(deleteBasketLineRequest)
        .then((response) => {
          commit('updateBasket', response.data);
        }).catch(error => console.log(error))
    },
    fetchBasket: ({commit, dispatch}, basketUUID) => {
      const fetchBasketRequest = `/api/v1/basket/${basketUUID}/`;
      return axios.get(fetchBasketRequest).then((response) => {
        commit('updateBasket', response.data)
      }).catch(() => dispatch('createBasket'))
    },
    deleteBasket: ({commit}) => {
      const deleteBasketRequest = `/api/v1/basket/${state.basket.basket_UUID}/`;
      return axios.delete(deleteBasketRequest).then(() => {
        commit('deleteBasket');
        commit('deleteBasketCookie');

      }).catch(error => console.log(error))
    },
    goToCheckout: ({dispatch}) => {
      return dispatch('fetchBasket', state.basket.basket_UUID).then(() => {
        return state.basket.checkout
      })
    },
  }
;

const mutations = {
  deleteBasketCookie: () => {
    window.$cookies.remove('basket')
  },
  updateBasket: (state, payload) => {
    state.basket = payload;
  },
  deleteBasket: (state) => {
    state.basket = null;
  }
};

const helpers = {
  checkBasketUUIDIsNullOrUndefined(basket) {
    return basket == null || basket.basket_UUID == null
  },
  initiateBasketAndAddProduct(dispatch, payload) {
    dispatch('initiateBasket')
      .then(() => {
        dispatch('addProductToBasket', payload)
      })
      .catch(error => console.log(error));
  },
  getProductIdsInBasket() {
    return state.basket && state.basket.lines ? state.basket.lines.map(basketline => basketline.product.id) : [];
  },
  updateProductQuantityPayload(product) {
    const currProductItem = state.basket.lines.find(basketline => basketline.product.id === product.id);
    const newQuantity = currProductItem.quantity + 1;
    return {'product': product, 'quantity': newQuantity};
  },
  getBasketLine(productID) {
    return state.basket ? state.basket.lines.find(basketline => basketline.product.id === productID) : null;
  }
};


export default {
  state,
  helpers,
  getters,
  actions,
  mutations
};
