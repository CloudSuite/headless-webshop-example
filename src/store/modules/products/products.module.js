import axios from 'axios';

const state = {
    productTotal: 0,
    productData: [],
    product: null
};

const getters = {
    getProductTotal: (state) => {
        return state.productTotal;
    },
    getProductById: (state) => (id) => {
        return state.productData.find((product) => product.id === parseInt(id))
    },
    getCurrentProduct: (state) => {
        return state.product ? state.product : {};
    },
    getAllProducts: state => {
        return state.productData;
    },
    getRangeOfProducts: (state) => (amount) => {
        return state.productData ? state.productData.slice(0, amount) : [];
    }
};

const actions = {
    fetchProducts: ({commit}, payload) => {
        /** Returns available products from the database **/
        commit('setAllProducts', []);
        return axios.get('/api/v1/product/', {params: payload})
            .then((response) => {
                if (response.data && response.data.count) {
                    commit('setTotalProducts', response.data.count);
                }
                if (response.data && response.data.results) {
                    commit('setAllProducts', response.data.results);
                }
            })
            .catch(error => console.log(error));
    },
    fetchProductById: ({commit}, productID) => {
        /** Returns product details from the database based on provided product id **/
        const fetchProductByIdRequest = `/api/v1/product/${productID}/`;
        return axios.get(fetchProductByIdRequest)
            .then(response => commit('setProduct', response.data))
            .catch(error => console.log(error))
    },
};


const mutations = {
    setTotalProducts: (state, total) => {
        state.productTotal = total;
    },
    setAllProducts: (state, products) => {
        state.productData = products;
    },
    setProduct: (state, product) => {
        state.product = product
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
