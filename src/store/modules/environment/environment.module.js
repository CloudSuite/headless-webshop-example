import axios from "axios";

const state = {
    enterpriseDomain: 'nl-nl',
    availableLanguages: [],
    shopLanguageCode: null,
    countries: [],
};

const getters = {
    getEnterpriseDomain: (state) => {
        return state.enterpriseDomain
    },
    getAvailableLanguages: (state) => {
        return state.availableLanguages
    },
    getShopLanguageCode: (state) => {
        return state.shopLanguageCode
    },
    getCountries: (state) => {
        return state.countries
    }
};

const actions = {
    updateShopLanguageCode: ({commit}, language) => {
        commit('setShopLanguageCode', language.code);
    },
    updateCountry: ({commit}, country) => {
        let newEnterpriseDomain = country.url.split('/').filter(item => item !== '');
        newEnterpriseDomain = newEnterpriseDomain[newEnterpriseDomain.length - 1];
        commit('setEnterpriseDomain', newEnterpriseDomain);
    },
    fetchAvailableLanguagesOfShop: ({commit}) => {
        return axios.get('/api/v1/domain/available-languages/')
            .then((response) => commit('setAvailableLanguages', response.data))
            .catch(error => console.log(error))
    },
    fetchCountries: ({commit}) => {
        return axios.get('/api/v1/domain/countries/')
            .then((response) => {
                commit('setAvailableCountries', response.data)
            }).catch(error => console.log(error))
    },
};

const mutations = {

    setEnterpriseDomain: (state, enterpriseDomain) => {
        state.enterpriseDomain = enterpriseDomain
    },
    setAvailableLanguages: (state, availableLanguages) => {
        state.availableLanguages = availableLanguages
    },
    setShopLanguageCode: (state, shopLanguageCode) => {
        state.shopLanguageCode = shopLanguageCode
    },
    setAvailableCountries: (state, countries) => {
        state.countries = countries
    },
};

export default {
    state,
    getters,
    actions,
    mutations
};