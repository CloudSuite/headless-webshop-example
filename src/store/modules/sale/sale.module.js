import axios from 'axios';

const state = {
    sale: null,
};

const getters = {
    getSale: (state) => {
        return state.sale;
    }
};

const actions = {
    fetchSale: ({commit}, saleToken) => {
        const fetchSaleRequest = `/api/v1/sale/${saleToken}/`;
        return axios.get(fetchSaleRequest)
            .then((response) => commit('setSale', response.data))
            .catch(error => console.log(error))
    }
};


const mutations = {
    setSale: (state, sale) => {
        state.sale = sale;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};