import axios from 'axios';

const state = {
    trees: [],
    tree: null
};

const getters = {
    getTrees: (state) => {
        return state.trees;
    },
    getHighestTrees: (state) => {
        if (state.trees.length > 0) {
            const rootTrees = state.trees.filter(tree => tree.parent_id === null && tree.children.length > 0);
            return rootTrees.map(tree => tree.children).reduce((arr1, arr2) => arr1 + arr2)
        }
        return [];
    },
    getTreeByID: (state) => (id) => {
        return state.trees.find(tree => tree.id === parseInt(id));
    }
};

const actions = {
    fetchTrees: ({commit}) => {
        return axios.get('/api/v1/tree/?limit=1000')
            .then((response) => {
                if (response.data && response.data.results) {
                    commit('setTrees', response.data.results)
                }
            })
            .catch((error) => console.log(error))
    },
    fetchTreeById: ({commit}, treeID) => {
        const fetchTreeByIDRequest = `/api/v1/tree/${treeID}/`;
        return axios.get(fetchTreeByIDRequest)
            .then(response => commit('setTree', response.data))
            .catch(error => console.log(error))
    },
    fetchRangeOfTrees: (context, childTreeIDs) => {
        if (state.trees.length > 0) {
            return state.trees.filter(tree => childTreeIDs.includes(tree.id))
        }
        return [];
    }
};

const mutations = {
    setTrees: (state, trees) => {
        state.trees = trees;
    },
    setTree: (state, tree) => {
        state.tree = tree;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}
