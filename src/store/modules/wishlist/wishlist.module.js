import axios from "axios";

const state = {
    wishlist: null
};

const getters = {
    getWishlist: state => {
        if (!state.wishlist) {
            return null;
        }
        return state.wishlist;
    },
    getTotalOfWishlistProducts: state => {
        if (!state.wishlist || !state.wishlist.products) {
            return 0;
        }
        return state.wishlist.products.length;
    }
};

const actions = {
    getWishlists: ({commit, rootGetters}, payload) => {
        let user_token = rootGetters.getUserToken

        let product_ids = [];
        if (payload) {
            product_ids = [payload];
        }

        return axios.post('/api/v1/wishlist/', {'products': product_ids}, {
            headers: {
                Authorization: "Bearer " + user_token
            }
        })
            .then(response => {
                commit('updateWishlist', response.data);
            })
            .catch(error => console.log(error));
    },
    addToWishlist: ({commit, dispatch, rootGetters}, payload) => {
        let user_token = rootGetters.getUserToken

        if (!state.wishlist) {
            dispatch('getWishlists', payload.product.id);
        } else {
            commit('addToWishlist', payload);
            let product_ids = [];
            for (let product of state.wishlist.products) {
                product_ids.push(product.id);
            }

            return axios.patch(`/api/v1/wishlist/${state.wishlist.id}/`, {'products': product_ids}, {
                headers: {
                    Authorization: "Bearer " + user_token
                }
            })
                .then(response => {
                    commit('updateWishlist', response.data);
                })
                .catch(error => console.log(error));
        }
    },
    removeFromWishlist: ({commit, rootGetters}, payload) => {
        let user_token = rootGetters.getUserToken
        let product_ids = [];
        for (let product of state.wishlist.products) {
            console.log(product.id, payload.product.id);
            if (payload && payload.product && product.id !== payload.product.id) {
                product_ids.push(product.id);
            }
        }

        return axios.patch(`/api/v1/wishlist/${state.wishlist.id}/`, {'products': product_ids}, {
            headers: {
                Authorization: "Bearer " + user_token
            }
        })
            .then(response => {
                commit('updateWishlist', response.data);
            })
            .catch(error => console.log(error));
    }
};

const mutations = {
    addToWishlist: (state, payload) => {
        if (state.wishlist) {
            state.wishlist.products.push({
                id: payload.product.id,
                name: payload.product.name,
                code: payload.product.code,
                slug: payload.product.slug
            });
        }
    },
    updateWishlist: (state, payload) => {
        state.wishlist = payload;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};