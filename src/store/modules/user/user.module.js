import axios from 'axios'

const state = {
    user_token: null,
    user_data: null
};

const getters = {
    getUserToken: (state) => {
        return state.user_token;
    },
    getUserData: (state) => {
        return state.user_data;
    }
};

const actions = {
    login: ({commit, dispatch}, payload) => {
        return axios.post('/api/v1/authorization/login/', {
            "password": payload.password,
            "email": payload.email
        })
            .then(response => {
                if (response.data && response.data.token) {
                    const user_token = response.data.token;
                    commit('updateUser', user_token);
                    dispatch('userData', user_token);
                    window.$cookies.set('cs_headless', user_token, 0, '/', window.location.hostname);

                    return
                }

                return 'failure'
            })
            .catch(error => {
                console.log(error.data)
                console.log(error.data.message)

                if (error.data.message) {
                    return 'You are a failure'
                }
                console.log(error)
            });
    },
    logout: ({commit}) => {
        commit('deleteUserCookie');
        commit('deleteUser');

        axios.get('/account/logout/')
            .then(() => {
                console.log(`Logout succeeded`)
            })
            .catch(error => console.log(`Logout error`, error));
    },
    userToken: ({commit}, payload) => {
        commit('updateUser', payload)
    },
    userData: ({commit, state}, payload) => {
        if (!state.user_token && window.$cookies.get('cs_headless')) {
            commit('updateUser', window.$cookies.get('cs_headless'))
        }

        return axios.get('/api/v1/user/user/', {
            headers: {
                Authorization: "Bearer " + payload
            }
        })
            .then(response => {
                commit('updateUserData', response.data);
            })
            .catch(error => console.log(error));
    }
};

const mutations = {
    deleteUserCookie: () => {
        window.$cookies.remove('cs_headless', '/', window.location.hostname);
        window.$cookies.get('cs_headless')
    },
    updateUser: (state, payload) => {
        state.user_token = payload;
    },
    updateUserData: (state, payload) => {
        state.user_data = payload;
    },
    deleteUser: (state) => {
        if (window.$cookies.get('cs_headless')) {
            console.log('cs_headless cookie still exists')
            window.$cookies.remove('cs_headless', '/', window.location.hostname);
        }

        state.user_token = null;
        state.user_data = null;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
