import Vue from 'vue';
import Vuex from 'vuex';

import userModules from './modules/user/user.module';
import productModule from './modules/products/products.module';
import basketModule from './modules/basket/basket.module';
import treesModule from './modules/trees/trees.module';
import environmentModule from './modules/environment/environment.module';
import saleModule from './modules/sale/sale.module';
import wishlistModule from './modules/wishlist/wishlist.module';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        environmentModule,
        userModules,
        productModule,
        basketModule,
        treesModule,
        saleModule,
        wishlistModule
    }
});
