import Vue from 'vue'
import VueCookies from 'vue-cookies'
import App from './App.vue'
import router from './router'
import store from './store/store'
import Vue2Filters from 'vue2-filters'
import BootstrapVue from 'bootstrap-vue'
import interceptorsSetup from "./shared/axios.interceptors";

interceptorsSetup()

Vue.use(VueCookies)
Vue.use(BootstrapVue)
Vue.use(Vue2Filters)
Vue.config.productionTip = process.env.NODE_ENV === 'development'


new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
