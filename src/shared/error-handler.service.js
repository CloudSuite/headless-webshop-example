export default function errorResponseHandler(error, router) {
  router.push({name: 'error',
    params: {
      status: error.response.status,
      statusText: error.response.statusText,
      responseData: error.response.data['message']
    }
  })
}
