import axios from 'axios';
import store from '../store/store';
import router from '../router';
import errorResponseHandler from "./error-handler.service";

export default function interceptorsSetup() {
  axios.interceptors.request.use(function (request) {
    const language = store.getters.getShopLanguageCode;
    if (language) {
      request.headers['CloudSuite-Shop-Language'] = language
    }
    const enterpriseDomain = store.getters.getEnterpriseDomain;
    request.url = `/${enterpriseDomain}${request.url}`;
    return request
  }, function (err) {
    return Promise.reject(err);
  });

  axios.interceptors.response.use(function (response) {
    return response
  }, function (error) {
    if (error.response.data['message'].toLowerCase().indexOf('sale') !== -1) {
      errorResponseHandler(error, router);
    }
    return error
  });
}
