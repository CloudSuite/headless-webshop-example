export default function getProductImage(product, type, size='normal') {
  let image = product.pictures.find(picture => picture.type === type);

  switch (size) {
    case 'mini':
      if (image && image.urls.mini) {
        return image.urls.mini;
      }

      return require('@/assets/default-product-image-mini.png');
    case 'large':
      if (image && image.urls.large) {
        return image.urls.large;
      }

      return require('@/assets/default-product-image-large.png');
    default:
      if (image && image.urls.normal) {
        return image.urls.normal;
      }

      return require('@/assets/default-product-image.png');
  }
}
