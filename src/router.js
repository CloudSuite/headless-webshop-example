import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/home.vue'
import Products from './views/products/products-page.vue'
import ProductDetails from './views/products/product-details.vue'
import ProductTree from './views/products/product-tree.vue'
import Basket from './views/basket.vue'
import Wishlist from './views/wishlist.vue'
import Confirmation from './views/confirmation.vue'
import Error from './views/error.vue'

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/products',
      name: 'products',
      component: Products,
    },
    {
      path: '/products/:treeID/',
      name: 'product-tree',
      component: ProductTree,
    },
    {
      path: '/product/:productID',
      name: 'details',
      component: ProductDetails,
    },
    {
      path: '/basket',
      name: 'basket',
      component: Basket,
    },
    {
      path: '/wishlist',
      name: 'wishlist',
      component: Wishlist,
    },
    {
      path: '/confirmation',
      name: 'confirmation',
      component: Confirmation,
    },
    {
      path: '/error/:status',
      name: 'error',
      component: Error,
      props: true
    }
  ],
});
