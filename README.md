# headless-webshop-example
In this project we have created a reference Front-end for the headless API of Cloudsuite. This is an example of how a developer could create a web shop that interacts with the headless api.

## Project setup
Create .env file and add: 
`DEVSERVER_PROXY_TARGET=http://localhost:5001`
This is to setup your proxy server.
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
